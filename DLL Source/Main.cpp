#ifdef WIN32
#include <Windows.h>
#endif 

#include <memory>

#define DLL_API __declspec(dllexport)

// Encapsulate the game window
class CManipulatedWindow {
	HWND mHWND;
	int mWidth, mHeight;

	// Since we can't capture in the WNDENUMPROC callback, we'll pass this struct as the LPARAM.
	struct enumProcData {
		DWORD processID;
		CManipulatedWindow* win;
		enumProcData(DWORD _id, CManipulatedWindow* _pWin) : processID(_id), win(_pWin) { ; }
	};
	
    bool CheckSize()
    {
        if (mHWND == NULL) {
            FindWindowHandle();
            if (mHWND == NULL)
                return false;
        }

        if (mHWND != NULL) {
            HWND parent(GetParent(mHWND));
            while (parent != NULL && parent != mHWND) {
                CloseWindow(mHWND);
                mHWND = parent;
                parent = GetParent(mHWND);
            }
        }

        RECT rcClient, rcWindow;
		GetClientRect(mHWND, &rcClient);
		GetWindowRect(mHWND, &rcWindow);
		mWidth = rcClient.right;
		mHeight = rcClient.bottom;

		if ((GetWindowLongPtr(mHWND, 0) & WS_OVERLAPPEDWINDOW) == WS_OVERLAPPEDWINDOW) {
			int w = rcWindow.right - rcWindow.left;
			int h = rcWindow.bottom - rcWindow.top;

			mWidth = w - mWidth;
			mHeight = h - mHeight;
		}

        return true;
	}

    void FindWindowHandle() {
        enumProcData data(GetCurrentProcessId(), this);	// Get the current running processID, the GM game executable
        EnumWindows([](HWND _hWnd, LPARAM lParam)->BOOL {	// Loop through each top level window
            enumProcData& data = *reinterpret_cast<enumProcData*>(lParam);
            DWORD processId = data.processID;
            CManipulatedWindow* fakeThis = data.win;
            DWORD windowProcessId = 0;
            GetWindowThreadProcessId(_hWnd, &windowProcessId); // Is this hwnd the one that matches our executable?
            if (windowProcessId == processId) {
                fakeThis->SetWindowHandle(_hWnd);
                return FALSE; // we're done, stop looping
            }

            return TRUE;
        }, reinterpret_cast<LPARAM>(&data));
    }

public:
	void SetWindowHandle(HWND _hwnd) { mHWND = _hwnd; }

    CManipulatedWindow()
    {
        mWidth = 0;
        mHeight = 0;
        mHWND = NULL;
    }

    bool SetBorderless()
    {
        if (!CheckSize())
            return false;

        SetWindowLongPtr(mHWND, GWL_STYLE, WS_VISIBLE | WS_POPUP);  // WS_POPUP removes the border
        SetWindowPos(mHWND, NULL, 0, 0, mWidth, mHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED);
        
        return true;
	}

	bool SetWindowed()
	{
		if (!CheckSize())
            return false;

		SetWindowLongPtr(mHWND, GWL_STYLE, WS_VISIBLE | WS_OVERLAPPEDWINDOW); // WS_OVERLAPPED is our traditional frame style
		SetWindowPos(mHWND, NULL, 0, 0, mWidth, mHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED);

        return true;
	}
};

static CManipulatedWindow GameWindow;

extern "C" {
    DLL_API double SetWindowHandle(char *_windowHandle) {
        GameWindow.SetWindowHandle(reinterpret_cast<HWND>(_windowHandle));
        return 1.0;
    }

	DLL_API double SetBorderless()
	{
        return GameWindow.SetBorderless() ? 1.0 : 0.0;
	}
	
	DLL_API double SetWindowed()
	{
        return GameWindow.SetWindowed() ? 1.0 : 0.0;
	}
}