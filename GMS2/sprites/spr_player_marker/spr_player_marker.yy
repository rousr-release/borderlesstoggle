{
    "id": "925d7cd9-d576-41bd-8be2-98b0ff534bb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "806a624b-b504-4548-ad42-7f057cebe9bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "925d7cd9-d576-41bd-8be2-98b0ff534bb4",
            "compositeImage": {
                "id": "910bc87a-13d6-4237-a73d-bf73a82c9095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "806a624b-b504-4548-ad42-7f057cebe9bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b71b5a-21d1-47aa-ace9-d24005f6a41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "806a624b-b504-4548-ad42-7f057cebe9bf",
                    "LayerId": "7e07559e-bf62-4f64-a857-3d21d76d726e"
                },
                {
                    "id": "01cf5ad3-9923-43c4-8c9f-4fd7afa8892c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "806a624b-b504-4548-ad42-7f057cebe9bf",
                    "LayerId": "53ffc8db-d366-4dd9-a3f2-980d5f4001f1"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 24,
    "layers": [
        {
            "id": "7e07559e-bf62-4f64-a857-3d21d76d726e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "925d7cd9-d576-41bd-8be2-98b0ff534bb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "53ffc8db-d366-4dd9-a3f2-980d5f4001f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "925d7cd9-d576-41bd-8be2-98b0ff534bb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 23
}