{
    "id": "662eaae7-6b62-48f9-8777-573f08e1abd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_open",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 351,
    "bbox_left": 32,
    "bbox_right": 607,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1c8c6402-467a-45a7-8e76-4d8ad0dd8705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "compositeImage": {
                "id": "6d1af11a-e30f-439c-a0c6-a12dc53eca95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8c6402-467a-45a7-8e76-4d8ad0dd8705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dae14ce0-c0cd-4cf6-a141-aca11bc5288b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8c6402-467a-45a7-8e76-4d8ad0dd8705",
                    "LayerId": "fb741898-d892-4a5b-b1e1-3f648b71b769"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 360,
    "layers": [
        {
            "id": "fb741898-d892-4a5b-b1e1-3f648b71b769",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}