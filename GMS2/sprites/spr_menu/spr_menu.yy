{
    "id": "48025201-64f0-41d1-aa53-e0aecfde02e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "72af9856-1c55-4b66-a8af-23cc18f6e5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48025201-64f0-41d1-aa53-e0aecfde02e0",
            "compositeImage": {
                "id": "02b6ffd7-08ef-4a77-8e90-2465742fbfc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72af9856-1c55-4b66-a8af-23cc18f6e5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371a7ca6-86a9-458e-8fd1-0714a5dc814a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72af9856-1c55-4b66-a8af-23cc18f6e5a7",
                    "LayerId": "9a6f9ae4-359b-4458-8983-091300394a2c"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 100,
    "layers": [
        {
            "id": "9a6f9ae4-359b-4458-8983-091300394a2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48025201-64f0-41d1-aa53-e0aecfde02e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}