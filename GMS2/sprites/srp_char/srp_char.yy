{
    "id": "ceaa7eaf-9681-444e-b860-439494d2dbb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "srp_char",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bd8f1a1f-76a0-4494-a6ed-f2c26316535e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceaa7eaf-9681-444e-b860-439494d2dbb0",
            "compositeImage": {
                "id": "c5269edd-3519-4d3e-abc9-c223413510c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8f1a1f-76a0-4494-a6ed-f2c26316535e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28f764f3-c702-4aa1-be24-08cbc1bdb425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8f1a1f-76a0-4494-a6ed-f2c26316535e",
                    "LayerId": "f7c3e31b-e407-4cef-b5f7-55cb892ada88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7c3e31b-e407-4cef-b5f7-55cb892ada88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceaa7eaf-9681-444e-b860-439494d2dbb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}