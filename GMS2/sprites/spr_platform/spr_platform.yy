{
    "id": "c0a38139-2058-4143-a44f-947bd6f0f6d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3d1a1ced-b289-4253-815e-59c0b754b737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0a38139-2058-4143-a44f-947bd6f0f6d6",
            "compositeImage": {
                "id": "c2aed12f-3682-4d27-a60a-c0c8dea0de1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1a1ced-b289-4253-815e-59c0b754b737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e7ab84-a208-4833-b7f7-0d9c198213c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1a1ced-b289-4253-815e-59c0b754b737",
                    "LayerId": "40d010ff-4229-425f-83aa-436d47f1a8a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40d010ff-4229-425f-83aa-436d47f1a8a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0a38139-2058-4143-a44f-947bd6f0f6d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}