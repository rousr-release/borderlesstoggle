#define __BT_init
__BT_setWindowHandle(window_handle());

#define bt_set_borderless
///@func bt_set_borderless()
///@desc sets the window to be a borderless window
__BT_preserveWindowSize();
__BT_setBorderless();

#define bt_set_windowed
///@func bt_set_windowed()
///@desc Set the window back to having a frame
__BT_preserveWindowSize();
__BT_setWindowed();  

#define __BT_preserveWindowSize
///@func __BT_preserveWindowSize
///@desc internal function to make sure the window stays the appropriate size
window_set_min_width(window_get_width());
window_set_min_height(window_get_height());

window_set_max_width(window_get_width());
window_set_max_height(window_get_height());
