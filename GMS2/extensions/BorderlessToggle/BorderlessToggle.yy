{
    "id": "bbd3d344-ba08-489e-835d-0d41d0fbf559",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "BorderlessToggle",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 64,
    "date": "2017-33-11 12:05:40",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "ebfd5e3a-4cbc-41ea-9101-6433250eaff6",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 64,
            "filename": "BorderlessToggle.dll",
            "final": "",
            "functions": [
                {
                    "id": "6d9622eb-dcec-4f61-ba14-d34dc2965119",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "SetBorderless",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__BT_setBorderless",
                    "returnType": 2
                },
                {
                    "id": "03184171-661b-4f11-bbd3-602d4126976f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "SetWindowed",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__BT_setWindowed",
                    "returnType": 2
                },
                {
                    "id": "a1cba1dd-eb6d-4b4d-b586-5e6a794b1813",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "SetWindowHandle",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__BT_setWindowHandle",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                "6d9622eb-dcec-4f61-ba14-d34dc2965119",
                "03184171-661b-4f11-bbd3-602d4126976f",
                "a1cba1dd-eb6d-4b4d-b586-5e6a794b1813"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "b0a2b499-9e04-4dc9-8238-35b14eb8226c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 64,
            "filename": "BorderlessToggle.gml",
            "final": "",
            "functions": [
                {
                    "id": "6da8e1d8-04d7-48fe-9d7e-815082c0375d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "bt_set_borderless",
                    "help": "sets the window to be a borderless window",
                    "hidden": false,
                    "kind": 2,
                    "name": "bt_set_borderless",
                    "returnType": 2
                },
                {
                    "id": "67fb34b0-d1cc-493f-aafa-d4aadb7314d0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "bt_set_windowed",
                    "help": "Set the window back to having a frame",
                    "hidden": false,
                    "kind": 2,
                    "name": "bt_set_windowed",
                    "returnType": 2
                },
                {
                    "id": "8214b196-b238-491d-8291-cbd824f652dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__BT_preserveWindowSize",
                    "help": "internal function to make sure the window stays the appropriate size",
                    "hidden": false,
                    "kind": 2,
                    "name": "__BT_preserveWindowSize",
                    "returnType": 2
                },
                {
                    "id": "dc023c5e-6d53-425c-b7d7-bf989fd30ce1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__BT_init",
                    "help": "set window handle",
                    "hidden": false,
                    "kind": 2,
                    "name": "__BT_init",
                    "returnType": 1
                }
            ],
            "init": "__BT_init",
            "kind": 2,
            "order": [
                "6da8e1d8-04d7-48fe-9d7e-815082c0375d",
                "67fb34b0-d1cc-493f-aafa-d4aadb7314d0",
                "8214b196-b238-491d-8291-cbd824f652dd",
                "dc023c5e-6d53-425c-b7d7-bf989fd30ce1"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.1.0"
}