# Borderless Toggle Extension

Using this extension, a GMS2 application can now toggle between a regular Windowed mode and Borderless Windowed mode.

Usage:

![Project Settings](http://forum.rou.sr/assets/uploads/files/1494796558081-upload-f27f8205-30c6-41d6-9d26-7fb0f02bd33d.png)

* Set your project to use Borderless Windows, and make sure you don’t have “Allow player to resize” checked.
* Call `bt_set_borderless` and `bt_set_windowed` and off we go!

### Credits

Font: 04b03  
Bloom Shader: https://marketplace.yoyogames.com/assets/4729/simple-bloom-shader

demo by [@net8floz](http://www.twitter.com/net8floz)
extension by [@babyj3ans](http://www.twitter.com/babyj3ans) of [rou.sr](http://www.rou.sr)

