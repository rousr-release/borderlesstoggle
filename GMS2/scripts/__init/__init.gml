//Helper for keeping track of window state for the demo
enum window_modes{
	windowed,
	fullscreen,
	borderless,
	borderless_fullscreen
}