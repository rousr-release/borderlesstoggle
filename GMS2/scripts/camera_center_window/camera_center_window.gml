/// @description Triggers the alarm to center the window

//Gms likes to wait a step before calling window_center()
show_debug_message("Tell camera to center");
game.camera.alarm[0] = 1;