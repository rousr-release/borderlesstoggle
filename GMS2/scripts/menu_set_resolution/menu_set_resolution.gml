/// @param string widthXheight
// Just a quick script for calling the menu option actions 
var _native_x = game.camera.native_resolution_x,
	_native_y = game.camera.native_resolution_y,
	_last_zoom = game.camera.zoom;

//String comparison from menu 
switch(argument0){
	case "640x360":
		camera_set_resolution(_native_x,_native_y,640,360);
	break;
	case "800x600":
		//Scale view with to non-native aspect ratio
		var _ratio = 800/600;
		camera_set_resolution(round(_native_y * _ratio),_native_y,800,600);
	break;
	case "1280x720":
		camera_set_resolution(640,360,1280,720);
	break;
	case "1360x768":
		//Scale view with to non-native aspect ratio
		var _ratio = 1360/768;
		camera_set_resolution(round(_native_y * _ratio),_native_y,1360,768);	
	break;
	case "1440x900":
		//Scale view with to non-native aspect ratio
		var _ratio = 1440/900;
		camera_set_resolution(round(_native_y * _ratio),_native_y,1440,900);	
	break;
	case "1600x900":
		//Scale view with to non-native aspect ratio
		var _ratio = 1600/900;
		camera_set_resolution(round(_native_y * _ratio),_native_y,1600,900);	
	break;
	case "1920x1080":
		camera_set_resolution(640,360,1920,1080);
	break;
}


