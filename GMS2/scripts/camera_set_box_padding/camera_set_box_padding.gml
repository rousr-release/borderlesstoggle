/// @param padding_x

//Padding is used for the camera bounds box that keeps all the players on screen
with(game.camera){
	padding_x = argument0;
	padding_y = padding_x * resolution_output_x/resolution_output_y;
}