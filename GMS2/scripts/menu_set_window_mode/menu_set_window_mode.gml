/// @param string
// Just a quick script for calling the menu option actions 
var _native_x = game.camera.native_resolution_x,
	_native_y = game.camera.native_resolution_y,
	_display_width = display_get_width(),
	_display_height = display_get_height(),
	_ratio = _display_width / _display_height,
	_last_mode = game.camera.window_mode;
		
//Call the extension if a windowed-style enum comes up
if(argument0 == "Window" || argument0 == "Fullscreen"){
	bt_set_windowed();
}else{
	bt_set_borderless();
}

switch(argument0){
	case "Borderless Window":
	case "Window": 
		//Sets the proper window mode state
		game.camera.window_mode = (argument0 == "Window") ? window_modes.windowed : window_modes.borderless;
		
		//Shouldn't be fullscreen! 
		window_set_fullscreen(false);
			
		//Hack way to get the menu option selected by the user for the resolution option
		var _res_options = obj_menu.options[2],
		_res = _res_options[obj_menu.options_index[2]]; // Returns a string like "640x360"
			
		//Fullscreen has it's own output_resolution settings - so revert back to 
		//the last chosen window size when switching
		menu_set_resolution(_res);
	break;
	case "Fullscreen":
	case "Borderless Fullscreen":
		//Sets the proper window mode state
		game.camera.window_mode = (argument0 == "Fullscreen") ? window_modes.fullscreen : window_modes.borderless_fullscreen;
		
		//Set fullscreen if the case is "Fullscreen"
		window_set_fullscreen(argument0 == "Fullscreen");
		
		//Set resolution to a scaled view of the user's display. Check the pixelated pope resolution video
		//for more information about scaling view to aspect ratio
		camera_set_resolution(round(_native_y * _ratio),_native_y,_display_width,_display_height);
	break;
}

