/// @description Resizes the application surface, window, viewport, and view
/// @param native_res_x 
/// @param native_res_y
/// @param res_x 
/// @param res_y 

with(game.camera){
	//Update demo variables
	resolution_x = argument0; //view_x
	resolution_y = argument1; //view_y
	resolution_output_x = argument2; //window_x
	resolution_output_y = argument3; //window_y

	//Set the viewport
	view_set_wport(camera,resolution_output_x);
	view_set_hport(camera,resolution_output_y);
	
	//Set the window size 
	window_set_size(resolution_output_x,resolution_output_y);
	
	//The BT extension requires that we set our min width / height when changing the window size
	window_set_min_width(resolution_output_x);
	window_set_min_height(resolution_output_y);
	
	//Set the view 
	camera_set_view_size(camera,resolution_x,resolution_y);
	
	//Reisze the application surface 
	surface_resize(application_surface,resolution_output_x,resolution_output_y);
	
	//Set GUI size 
	display_set_gui_size(resolution_output_x,resolution_output_y);
	
	//Center the window ( waits one step ) 
	camera_center_window();
	
	
	//Try and set camera to a similar zoom as it was before 
	camera_set_view_size(
		camera,
		camera_get_view_width(camera) / zoom,
		camera_get_view_height(camera) / zoom
	);
}