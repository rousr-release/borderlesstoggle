{
    "id": "be3fea18-4829-4f7a-91c9-98e60b02e5b1",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_8px",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0e3004ff-cd63-4f6c-93da-6d0ffc50b94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "76f2e8f3-c29b-434b-8e70-9877e229bd56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 18,
                "y": 67
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fbfed2a3-f048-4039-bf01-be64ebeb0fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 4,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 12,
                "y": 67
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dd49e12f-2df6-40c5-a308-0f1b98264387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c0cfebf3-6973-4d79-94e8-d9832096f1b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 15
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "636a1e87-7b43-4a5f-a83c-a7c4627c6dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3acfbe48-ae63-48f0-9d38-fb283a764c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1a871436-832b-406e-90b9-767521f8b4ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 4,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 39,
                "y": 67
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "eb3e7b17-bbee-4aaf-9020-0a11bc9d58cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 73,
                "y": 54
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3c3492d2-69a6-4bb8-806c-830f5473bb3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 93,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6ad5fe5c-c7de-4bc8-b5e2-f89e829acea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 106,
                "y": 54
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "04f2f7ae-9a41-46b6-be56-2c580ed070e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ccff44cb-140a-4cea-9e03-21a3d37b4269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "63c70d1a-4e06-4570-8c81-4e2c351dfe44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 112,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e0ff8931-efb7-4ec8-b5aa-5b213eb2a320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 118,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e9768b1a-33e0-4612-8195-a436d67dc3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2d049c95-560b-4f31-b840-a1ccf4408b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "11001228-57be-4d2c-8436-879f5f3e22a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 78,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c8c9c2b2-1213-4bc4-87a8-171018618d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 28
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "88767686-8cef-4574-b56b-ebea0d6bb18a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "94e7a275-97cb-408e-9056-f4b19b2330da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 28
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bf331a28-5a46-4daa-809a-001987291faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 28
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6c471c90-7568-4420-a203-8c7d1ac08020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 41
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8ef47cc1-8aff-4f76-b14a-bbac50f2d54a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 28
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "915df40f-6b28-4b56-9762-20c90e78492a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 41
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "984064d6-02ee-4813-8869-5aa14da0d005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 41
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6c5718dc-f0d8-4455-88d4-c7a6c75f4705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 7,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 30,
                "y": 67
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bec5336e-878a-4520-8794-d22ae9616549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 26,
                "y": 67
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d331d505-49a3-4f9f-9a19-f3e14bdc6435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 20,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bb2018d2-5c5c-4873-af8e-720fe3eb2191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 56,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "995d66ea-ffc2-434d-a963-01ec09516c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e7caaf10-4f15-4c4b-ad86-b96ce8744dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 15
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b2434681-73c8-4ab4-9a32-62dfb4d468ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "90ccd91c-75ea-4415-84c4-aa11d0641702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 41
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2d58640f-e588-476e-92cb-4ff903dee073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 41
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8737a8e0-e5b7-473c-b6a7-a50f6bb3baec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e9bfaf55-c059-4d75-8941-7278bdb6dd7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5441aafb-c0ed-492a-bdfd-c3c3bd2b9cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 32,
                "y": 54
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6d234396-5316-4c79-ac52-ec50eb1d9fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5e304f4d-f797-47d9-bd9d-2d2db0d68ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 41
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fd3bca22-c9bf-44f9-af4d-1b7cfc707c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 73,
                "y": 15
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a1369210-e38e-4c2f-a5d5-7be781c179be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 54
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a95b25a5-f341-4c9b-826f-6272c98d623e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 81,
                "y": 15
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "40e97974-96d8-4d4a-a1b1-ad9f8c0ddbdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 89,
                "y": 15
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1f95790e-7335-432f-8688-9a07758a4bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 83,
                "y": 41
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2dc381f2-0a81-444b-81a2-f330d3314eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0e5decad-c41b-4dbd-990b-0bf917c07963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "711b9a21-e99c-4a96-bbcd-3f5ffc9fe570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2a0ad7ba-a91c-4837-9da4-46f0e21ec8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 89,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cfb39994-857a-4449-9283-db5b354ec66d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 82,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b79a2363-c96e-4699-8de8-b0a3ac5e56a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 103,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "86ddbeab-d6bd-431e-a393-98f7742e643c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 15
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a3655fb7-a552-4585-b171-8b9b7b7f0663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 54
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8be223ff-a473-4f69-89ef-b368d44501d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 15
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cf291311-2472-4d7d-a713-8adeaca0b82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 15
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1708d53c-06b1-4528-b21d-0d4c94552e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2c56ec2e-48f6-47ab-9231-373bc087897f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 15
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d693cf0c-343b-41a1-919d-176b31247e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 25,
                "y": 15
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "06eee364-6e03-413c-a1f2-156fcee310f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "25da48a6-588c-4b8b-99d6-4eff9c9736e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 83,
                "y": 54
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "695d5091-1e3b-4a3d-8c9d-4da236c3c4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0118c93d-b2db-4b8e-b7db-08fc22ec96aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 54
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "505fdb61-3e92-417f-8591-20f053d57a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 4,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 6,
                "y": 67
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1bd1ab25-ca05-4d57-94bf-b03f026b898c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 105,
                "y": 15
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0d771256-e558-4d9b-b98c-be7df8b76a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 4,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 34,
                "y": 67
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "def48e22-6714-422a-91d3-c3b3ed7a0f17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 110,
                "y": 28
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "51490207-26d0-491f-922f-04823753458a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2acce0df-d608-4213-92be-a4711831a5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 119,
                "y": 41
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "597c293c-90fb-4960-9cf4-bb5b6cb5d673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 96,
                "y": 28
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "04e8b65c-e083-4f8e-9c64-0ad2d4027875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 77,
                "y": 41
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6672cbba-edff-41c7-b2c3-1619c0060fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "250f776a-1db9-47d7-8148-aa9660061ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "43225c0e-e2a5-423a-8a96-b2c135dc5e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 15
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b87ee5ed-8536-4337-90d1-7fe87b84a4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 67
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5fca385f-b587-4673-be4e-eb383c37d9a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 41
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ce62178a-a507-4834-aaca-1e46fd098051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 58,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a8a01f94-f3a9-4fba-a316-aadd64f18c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "54bb0ae8-7677-4bc2-afe2-41be35cceb64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9525ea9a-4a3f-4805-a502-26e2ca007f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "000fd8a7-f4c9-4883-8628-f13fe14aadc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 117,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "df8890af-9ae3-4d66-b2b8-1fd22fbf90b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "55f50d3a-60f3-4260-85d4-d16d806638e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8cd98378-7ec9-4a20-9f9e-76e5eb56bb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 113,
                "y": 41
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b8902b2d-2cc8-4a53-a88a-844b7ddc19e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9f93c1fc-07a5-43f5-9b42-b01c346d4c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 95,
                "y": 41
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f5713046-bc21-4180-a8ea-743f66e8d717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "99a7ad90-732f-49f5-a944-50bb0177cb18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 15
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d87e89b4-450f-4696-b0d9-f54c7d4a2f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "417303bd-2cf1-4da5-a888-4e8cc7069934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 101,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ee37e67e-f993-4703-81c3-1d9a1fe11fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c1f42c0a-ae38-438d-abf2-456f50565b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 15
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "85b0c38f-fdb7-44bd-ac82-4fcde1de68d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 107,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e0ec2ceb-a951-499a-abd4-62c94d174d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 22,
                "y": 67
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f857106d-4e42-4b06-a1ef-df2864cb9727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 89,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "57da1519-e915-4ad3-9885-58ae18f08785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 4,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 54
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}