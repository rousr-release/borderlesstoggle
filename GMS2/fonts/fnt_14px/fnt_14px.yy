{
    "id": "a2e19b5d-6deb-4313-9292-e9f8c18f7ff5",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_14px",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "95787f08-4cf0-41ec-a781-e71295892f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9c1b96f2-f5d1-4ad0-9192-358fd470e9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 199,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "dd5d7b51-0a44-414f-84f4-0655843e36cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 180,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5358b75b-31f0-4434-ad4c-aac4b175f1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d21d14ec-a124-4b7d-9032-0742b67739e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c97e8c5d-6ed4-4744-90c8-37f508a86ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "743b06d4-46d1-49bf-b898-bdeeae930a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3d6647a0-f9f9-4d9e-9b74-f0d4579ea1d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 231,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1837fc7b-f460-4cf6-a260-1395fbd84763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 127,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9d96bb76-30ee-4149-a0b6-55b38313d2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0b848a20-57a3-4cf0-a9c7-7e6a3b0baac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 153,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3678e2a7-f835-4cc6-91d3-1f98aa1d685c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "92034a29-3a85-458c-9421-80a7d7b84805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c6f47e01-d420-4a8b-8f31-90a702641afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 162,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7120dad2-76f4-44f2-b66f-ae23a56b712f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 214,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f9061ee7-6d44-4f02-9e8f-f272461c54fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "768a4843-3ea5-4455-b902-cf876aa0dab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 101,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b2af1ef7-689d-4bfd-aea9-041527deaebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 134,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "88e39a9a-a913-4867-a0bf-46420615915a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a86ffa5d-2fde-4e75-8f19-c2ee03c7cf94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0a432c4a-2697-46f5-b574-072ce0363c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c32f2e3d-2a03-422c-a5e2-3142bbcafd6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 23
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "104b3290-4006-4941-88b5-3b6c6d87160d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 134,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d6c700cf-8555-415b-8f48-9b16fbcc910c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "02773c04-509d-4e27-a5f7-a7d57ca5caec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 145,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a869d645-b082-4bc2-8c7f-e0deaf3db67c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "607c6a26-3e7c-4649-87ed-deb80a7e9fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 219,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2eb8fbdf-3132-44c6-a53e-375bff7104b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 204,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7b2c2aec-e4b8-4360-aa63-2784af009154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "81f4954b-4e9e-4f73-b1e8-07cc150eb254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 81,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0d06d502-e911-4333-ad98-8b2860ff0c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 56,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b9bdb947-ecec-4808-b9bb-055e0a7ba8a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "518d32b8-7e6e-467f-a02d-343886469d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8fa0a27a-a7e5-4e30-8f02-78be60370eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 123,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f19820b9-5b9b-4719-9416-ee9782336eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 112,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b959257e-ae4f-4466-a43a-413d9e6ebbd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "89f1c041-7910-4d09-966a-5d5cb5a77ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "71317eb4-ccfe-4676-a0be-77767b494b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f4a8c053-e7fd-48e9-8747-b0a3fec17798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f50e83e5-13ab-47b8-a2d9-f85b971b82d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fd6da7a7-ac77-4203-900f-cb18db0d25e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0b4baf0c-cbe6-4787-9fb9-83a096ce0128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "90cb99c5-3560-420b-8fc7-15c77dba007f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 23
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "577607d1-a528-4549-a435-61387566f023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4375c795-72ea-4e38-babc-fe9a39e8d14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 222,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0b750f67-98f6-496d-8f5f-724697920310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "349ef344-7d1c-4100-a65e-50fa07c7133c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "06d3ebb9-5495-4f44-8727-fbc279676180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "401f90c1-651d-4639-8699-62ffdfea5643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 24,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d5ee4141-6902-4470-8438-73d553db3c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "93e44681-3b00-4174-b073-c03b14cfd9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3ec24eb6-625f-49a9-ba2a-fdf770cdcf99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "00134eb0-c379-4e0b-a53e-d048c4cb22c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "719b92b4-92f2-4cc9-82f5-bdc7b512ceee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cb8a5e88-a616-4557-8fb6-936887e295fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 23
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cbe82074-1e49-49a1-a31a-e9af1fc973ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5ac61e85-a5b0-45ae-a36e-1ea1c50f4b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "66a14ab2-d1fb-4b15-b3be-6af2c81eadfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 23
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7a7fc93c-f685-4c00-8a49-c955e5ba25f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 47,
                "y": 65
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3c421071-a2be-4f6e-9bb8-724676f1d4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 113,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "63f4c5b7-1ecc-499f-aac2-060d4a2d6a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1cfc83cc-a442-49f5-9c88-7ed9341f738d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 120,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fd7a25da-85a2-4376-83a5-090993fd9e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 171,
                "y": 65
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "77fb4607-53a5-4f3c-836f-39bb2e656ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 23
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dc56a69d-d8dc-48dc-9be5-5bbf4c685be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 224,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "98d3c6cc-a2ac-4d58-9943-25ba828090fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 57,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "87b4820b-5673-4f11-8522-311cf1c45d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3e9d1f8d-d24c-4e57-938e-77567de3c21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "434f2763-5db7-48eb-b061-e52be9cda2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 13,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a9e92f59-fde3-4361-973f-fa2ec125778f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2962b84-8b2b-4431-a1dc-1e3324929a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b4aaaea7-a48d-4466-8bd8-62f5b287aa36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0fe0f243-c3e4-4f99-8a46-d39211e43a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ad5cf28d-8db8-41dd-b49d-a2a2f67c7227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 194,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1f43e08e-9784-4d3a-92c6-23068195c759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "396175ba-f9c3-4d94-b74f-fd83660c792f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0c11222c-4a61-403c-aee6-8e3ca5108e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 189,
                "y": 65
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "68c0300e-8ca3-43f4-a2f6-967b1b7a2b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "337abd8e-a9b3-42c5-b993-f93afbaab254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b1be15f1-5b27-4105-9f07-41e086e8bf28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 35,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0244ce89-7769-44ce-9fa5-6d3035beb23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e5ddafe6-ca3c-4331-a188-322f585ac468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "acc1e990-117e-4a1c-b358-ebb3bea981c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "669684b2-79d4-4496-8ce2-30e717348f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0425aadb-dc38-446f-aca0-9dbc7d44ea9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f1befff7-a0f3-4a4c-a327-a524e0657f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "56ca2873-e4eb-44d1-81b3-f6cd0da2e358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d229de8f-15ad-4de7-bf35-992c521d3f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fc8e50d6-21be-4ac4-ba86-8ae2a2a28242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 195,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5cbb2418-7183-4d0f-984d-2f1d56348bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c3cafed9-4eeb-4f08-94fb-e2205a6ee6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c78307c2-e41b-4693-9e8f-df723226bc4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 186,
                "y": 44
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "96f0bc1c-9311-4f82-9e15-1fc76faaed10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 209,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "df1aa716-1874-4b59-8b82-aed29355fbce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 177,
                "y": 44
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ede3c07f-0063-4fe6-bf78-463a81927ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 141,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}