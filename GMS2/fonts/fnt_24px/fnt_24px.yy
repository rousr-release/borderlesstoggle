{
    "id": "9b1c3d8c-806b-46c9-8bd5-80a78cd2827b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_24px",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a009d24e-eb06-4f84-93b2-b54a8496d928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "fe895afd-43af-4eab-9ffa-49cfb8fe18a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 165,
                "y": 157
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4c763bbe-ae24-46f2-b389-19d60552a723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 134,
                "y": 157
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7000f745-fa9e-42f6-8eb4-9ba9b697029a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "67f110d0-1611-4e31-a042-78bf9e34f7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 59,
                "y": 33
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "36162fa1-4b7a-4444-afc2-cf51eeb428a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a97c42c9-5526-49d7-9a27-d7a7a9a93cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 33
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "abd9e3d9-374e-4708-8681-1eb5384b72a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 199,
                "y": 157
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "366f4761-b5da-483c-bc08-bb618122221f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 38,
                "y": 157
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "044dbb30-2db0-481a-9387-d9fb07c03534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 58,
                "y": 157
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e3d4fb4a-9082-43ed-ae1b-d314be2db480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 78,
                "y": 157
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "cf72449f-c3fe-48b3-ae0f-88a0f1d04057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 2,
                "y": 157
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d85ad14e-d491-4492-95ad-1655104c49bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 236,
                "y": 126
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "83fb4bd9-0507-4ebc-8536-e75f824dec06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 108,
                "y": 157
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9f437947-e31e-4323-b2e4-6b3e936ac65b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 187,
                "y": 157
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "85625c62-9bcb-4f4e-8b18-d18022ea9a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "262e0385-200f-454b-9019-02db14cf0034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 189,
                "y": 95
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "77f99e7b-ff53-4503-8e60-d936304b803b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 28,
                "y": 157
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cf21de88-4f23-40b1-9d07-300d1972a175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "cad19580-982c-433a-ad09-04e19ba1390f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 104,
                "y": 95
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a10fc079-98bf-4b59-af96-d7c63ba540ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 87,
                "y": 95
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7970ee06-fdef-4cba-9a9e-0926f23a3858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 36,
                "y": 95
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c34c4344-2a09-4de4-b23f-d0bd80725cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 19,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cecc4a9c-a432-4a67-b6f4-6959e1ab6245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 155,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "42613699-a9f8-409d-9fd0-a819d9cca7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 121,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a090e244-5029-403f-9103-e1e550c3f06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 53,
                "y": 95
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e17a5ca1-189f-47d4-98e5-41b3322fc590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 193,
                "y": 157
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "711f791a-da06-44c7-a5d3-555a92146b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 159,
                "y": 157
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "99c77f5d-d39f-4df2-8da8-b63c91b87811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 187,
                "y": 126
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e84f5105-927b-4925-b14a-34816a26648b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 15,
                "y": 157
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "98cc3100-c6a2-4471-b2ff-e1fb9c2a5450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 109,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2bbc8040-20db-45ef-b57c-a8324c7f3c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 138,
                "y": 95
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1bbb3a56-5101-4526-bbdf-3c12be44fd27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9806747c-52fe-499e-a8ca-2c27d5d745cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 223,
                "y": 95
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1d239720-adfe-4ab5-a06a-7472daa6db08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 172,
                "y": 95
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7a026ae8-9a48-460a-b58e-66ef09b6037b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 135,
                "y": 126
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ec45743e-45d5-46b0-81dd-2a4ad36e43be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 206,
                "y": 95
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bc29eaca-660f-4c14-9bfd-fada5a152545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 161,
                "y": 126
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b4c84d2f-62e0-4205-8814-5bc0dbc73a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 122,
                "y": 126
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "eb4cbcc7-5fa5-4513-9e63-a7bd8119ca35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 70,
                "y": 95
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5325b09d-3ad3-4d19-8af6-4e1ff979358c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 206,
                "y": 64
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "05fd9709-1d59-4e57-bff6-593d4f6bb616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 83,
                "y": 126
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "83e3e06e-3794-4a17-845c-f3428e6dd06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 36,
                "y": 64
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d1f1709c-72ca-48af-93f5-182be4fd78a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 195,
                "y": 33
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "96514155-2200-46c9-a0d3-b5246326bb4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 200,
                "y": 126
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e6f1d759-fa31-480c-a9e2-e4cfb9032bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1a4778a1-ee7c-43da-8d97-9a8fa6e62d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 110,
                "y": 33
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d400426f-dd4f-4e67-8f0f-0ace9f833b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 223,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ee338924-e4a7-4fb6-9c98-48bcff5c79ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 93,
                "y": 33
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "17d6f7af-b1f5-496b-8171-b2c8f3baee19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 42,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4e84590f-28d8-4302-b400-bddd22b162ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 33
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f2e36490-8bac-4c2d-813e-770bb248b6bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 144,
                "y": 33
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "743363bb-a492-465f-9568-6e25ad6d1653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 213,
                "y": 126
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7199fc03-8564-459d-957a-df340b2e2912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 161,
                "y": 33
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1ad2217b-c3db-43d4-9230-463308c21c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 178,
                "y": 33
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "76d97fd3-7174-45ac-a5f9-0f2bfb9e0186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "21c285fc-029d-41ca-940d-3cecd54a8a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 76,
                "y": 33
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f02ce186-fbb8-4a53-a50b-5c7c8e374acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 189,
                "y": 64
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c9bc0912-2eae-4d91-b96d-c0befb50dc70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 148,
                "y": 126
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "961ad15d-6253-4fb1-9370-d88bc774cd6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 48,
                "y": 157
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "30636896-f93d-4c96-b030-2d2c55a0e95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6f6e22c0-022c-4c73-85bd-e4b666a06a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 68,
                "y": 157
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9a696512-773f-4899-a49e-a2bd805c0f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 121,
                "y": 157
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "858b0216-5a7f-4c73-8591-755fe2199f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 229,
                "y": 33
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c00b9a38-4204-4080-a6c7-fa6587dc9eb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 171,
                "y": 157
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c0744ffa-23c3-4429-bbfa-13e96eba49ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ff85592d-3cb9-4f46-a5b2-2bd1a55183d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 212,
                "y": 33
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5f483e60-b2db-4e67-a579-5cb9e992156a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 96,
                "y": 126
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "85bfdb6c-f0d0-48a3-aeea-3f5ecff11125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 19,
                "y": 64
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "12f6d79e-2f8c-4c1e-9967-d3cc7b4247bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "38579ddc-b550-4cbb-95bf-222fd9f91cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 18,
                "y": 126
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "db480fb0-eb59-4485-9390-98e49773489a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "822b3d35-06ec-40f2-b7a3-29ce34e8471c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 53,
                "y": 64
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d5c8aed1-5d0f-4a62-b3d3-06bac62e5835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 153,
                "y": 157
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "881abc69-bc7d-4ca6-a3ec-6110818d8c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 226,
                "y": 126
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5eef9252-9919-4082-8015-9b28e8f37ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 70,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0a741c9e-48f1-4c01-a1e1-4a897e632648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 147,
                "y": 157
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "06e7cc42-40e8-4d1e-91cb-3ad1f50bffb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "22caffcc-43c1-46da-b729-68fe73526402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 87,
                "y": 64
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1752a8ee-4d7b-4056-860d-e18e6bbf6d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 104,
                "y": 64
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6087ec63-e846-4be6-b092-6185a43c4fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e5d18d29-2697-4d5d-849b-90d86277cae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "84d10c87-c7fd-4e4b-b88a-cf582ce293ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 31,
                "y": 126
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "310e6841-82f4-4509-abfd-41edf0612b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 138,
                "y": 64
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "012708f5-44c4-4767-bf95-472ebf4cf75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 44,
                "y": 126
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "718f2c27-26c3-4d54-8e83-5c6cd5b06d63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 155,
                "y": 64
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ad5fcb48-69da-490d-9b67-6c8893974432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 172,
                "y": 64
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7bdf44d8-d4b7-4925-9e0f-48c2726f786d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4b3658f6-2e3d-4e57-add6-fc40caf2ae65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 57,
                "y": 126
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f5733873-39bd-48d9-8fbc-31a83e1cb33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7e17c77a-0959-4ae3-a588-5872840d84c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 121,
                "y": 64
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3b45333c-2033-4a78-8348-a23f835ebb6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 70,
                "y": 126
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4d4619f9-1978-43bb-8bb3-370eaa4000f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 181,
                "y": 157
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0d690dec-2013-4fd8-88fa-2147b39a1cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 174,
                "y": 126
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "68790c5f-1573-45d7-bf9f-c51620658bbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 91,
                "y": 157
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}