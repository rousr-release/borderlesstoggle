player = false; //Does the player control this?
vx = 0; // velocity vector x
vy = 0; // velocity vector y 
dir_ai = choose(-1,1); // variable for switching AI dir
dir_player = 1; // variable for what direction player is facing
grav = 0.8; // Gravity force

//Player overhead marker ( quick and dirty code ) 
player_has_moved = false;
marker_timer = 0;
marker_alpha = 1;
idle_timer = 0;

//Original x and y - used for setting the player back to the
//start when falling off the platform
ox = x;
oy = y;