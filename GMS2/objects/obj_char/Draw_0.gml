//Draw character
draw_self();

//Player overhead marker ( quick and dirty code) 
if(player && marker_alpha!=0){
	marker_timer = (marker_timer + 4) mod 360;
	if(player_has_moved){
		marker_alpha-=0.02;
		if(marker_alpha < 0 ){
			marker_alpha = 0;
		}
	}
	draw_sprite_ext(
		spr_player_marker,0,x,y-52 + (6 * dsin(marker_timer)),2,2,0,c_white,marker_alpha
	);
}