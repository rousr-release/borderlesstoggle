{
    "id": "43ab125c-8b6b-44f0-a4d5-c304128109be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_char",
    "eventList": [
        {
            "id": "87867804-e107-4e60-ac0d-4eec228bfd5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43ab125c-8b6b-44f0-a4d5-c304128109be"
        },
        {
            "id": "1297a4b6-4432-4400-8da7-f5de8a345d74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43ab125c-8b6b-44f0-a4d5-c304128109be"
        },
        {
            "id": "9262d9d4-c219-4848-94ce-b65929e77a45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "43ab125c-8b6b-44f0-a4d5-c304128109be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ceaa7eaf-9681-444e-b860-439494d2dbb0",
    "visible": true
}