//No move on pause
if(game.paused){
	exit;
}

//Apply gravity
vx = 0;
vy+=grav;

if(player){
	//Player Controls
	
	//Move
	var _dx = keyboard_check(vk_right)-keyboard_check(vk_left);
	var _speed = 3;
	vx=_dx * 3;
	if(vx!=0){
		dir_player = sign(vx);
		player_has_moved = true;
		idle_timer = 0;
	}else{
		//Player overhead marker ( quick and dirty code) 
		if(player_has_moved){
			if(idle_timer>2){
				player_has_moved = 0;
				marker_alpha = 1;
				marker_timer = 0;
			}else{
				idle_timer+=0.02;
			}
		}
	}
	
	//Jump
	if(place_meeting(x,y+1,obj_platform)){
		if(keyboard_check_pressed(vk_space)){
			vy-=20;
			player_has_moved = true;
		}
	}
	
}else{
	//AI Controls
	
	vx+=dir_ai * 1.3;
	if(!place_meeting(x+vx,y+1,obj_platform)){
		vx=-vx;
		dir_ai = -dir_ai;
	}
}

//Classic GM platforming collisions 
if(vx != 0){
	var _svx = sign(vx);
	
	if(place_meeting(x+vx,y,obj_platform)){
		while(!place_meeting(x+_svx,y,obj_platform)){
			x+=_svx;
		}
		vx=0;
	
	}
	x+=vx;
}

if(vy != 0){
	var _svy = sign(vy);
	if(place_meeting(x,y+vy,obj_platform)){
		while(!place_meeting(x,y+_svy,obj_platform)){
			y+=_svy;
		}
		vy=0;
	}
	y+=vy;
}


//Reset when falling off a ledge
if(y > room_height * 2){
	x = ox;
	y = oy;
	vx = 0;
	vy = 0;
	player_has_moved = false;
	marker_timer = 0;
	marker_alpha = 1;
}
