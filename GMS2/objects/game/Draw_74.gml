//Cool blurry stuff
shader_set(shdr_bloom);
shader_set_uniform_f(uniform_blur_size, 0.01);

if(game.paused){
	shader_set_uniform_f(uniform_bloom_intensity, 2);
}else{
	shader_set_uniform_f(uniform_bloom_intensity, 1);
}

draw_surface(application_surface, 0, 0);
shader_reset();
