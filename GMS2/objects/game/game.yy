{
    "id": "5514ea66-20da-461a-a93f-74f9abb7cf71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game",
    "eventList": [
        {
            "id": "d27151cb-2fea-4c75-a5f8-eabbd0cda190",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5514ea66-20da-461a-a93f-74f9abb7cf71"
        },
        {
            "id": "a727e740-9f14-4b31-a956-65877101fd32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "5514ea66-20da-461a-a93f-74f9abb7cf71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}