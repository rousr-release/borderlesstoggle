//Let wsad be updownleftright
keyboard_set_map(ord("A"),vk_left);
keyboard_set_map(ord("D"),vk_right);
keyboard_set_map(ord("W"),vk_up);
keyboard_set_map(ord("S"),vk_down); 

//Keep track of paused 
paused = true;

//Create the camera 
camera = instance_create_depth(0,0,0,obj_camera);


//Default camera settings 
camera_set_resolution(
	camera.native_resolution_x,
	camera.native_resolution_y,
	camera.native_resolution_x * 2,
	camera.native_resolution_y * 2
);

//Padding of inner bounding box max bounds relative to the window size 
camera_set_box_padding(200);

//Opening screen 
instance_create_depth(0,0,-1,obj_opening);

//Bloom shader
application_surface_draw_enable(0);
uniform_bloom_intensity = shader_get_uniform(shdr_bloom, "intensity");
uniform_blur_size = shader_get_uniform(shdr_bloom, "blurSize"); 