//Initialize variables

//Resolution game is designed for 
native_resolution_x = 640;
native_resolution_y = 360;

//Current view resolution 
resolution_x = native_resolution_x; 
resolution_y = native_resolution_y;

//Window size and viewport size 
resolution_output_x = native_resolution_x; 
resolution_output_y = native_resolution_y; 

//Camera pan / zoom speed 
camera_speed = 0.03;

//Enable views and set camera / surface settings
view_enabled = true;
camera = camera_create();
view_set_camera(0,camera);
view_set_visible(0,true);

//For smooth movement - this system uses a min bounding box rectangle 
//that calculates the minimum rectangle that all the characters fit in
target_x = x;
target_y = y;
target_width = camera_get_view_width(camera);
target_height = camera_get_view_height(camera); 

//Draw on top of everything for debug mode
depth = -1000;

//Window mode
window_mode = window_modes.borderless;

//This variable operates as "read only" - it's used to recalculate what zoom we should
//roughly be at after changing display settings
zoom = 1;

