//No move on pause
zoom = resolution_y / camera_get_view_height(camera);

if(game.paused){
	exit;
}

/// Loop through objects and find the min bbox ( red square ) that 
/// contains the objects bbox + padding 

var _left = room_width;
var _top = room_height;
var _right = -room_width;
var _bottom = -room_height;

with(obj_char){
	if(bbox_left - other.padding_x/2 < _left){
		_left = bbox_left - other.padding_x/2;
	}
	if(bbox_right + other.padding_x/2 > _right){
		_right = bbox_right + other.padding_x/2;
	}
	
	if(bbox_top - other.padding_y/2 < _top){
		_top = bbox_top - other.padding_y/2;
	}
	if(bbox_bottom + other.padding_y/2 > _bottom){
		_bottom = bbox_bottom + other.padding_y/2;
	}
}


//Find min box dimensions 
var _width = _right-_left;
var _height = _bottom-_top;

//Proposed new sizes to use - we can only use one 
//of them as our aspect ratio may not change 
var _new_height = _width  *  (resolution_y/resolution_x);
var _new_width = _height * (resolution_x/resolution_y);

//Find the smallest scale change
var _height_scale = resolution_y/_new_height;
var _width_scale = resolution_x/_new_width;

if(_width_scale < _height_scale){
	//Width is smaller so use width
	_width = _new_width;
}else{
	//Height is smaller so use height
	_height = _new_height;
}

//The middle point of the min bounding box is our cameras target 
//You may offset this 
target_x = _left;
target_y = _top;
target_width = _width;
target_height = _height;

var _wview = camera_get_view_width(camera);
var _hview = camera_get_view_height(camera);

var _speed = camera_speed; 

_width = lerp(_wview,target_width,_speed);
_height = lerp(_hview,target_height,_speed);

x = lerp(x,target_x,_speed);
y = lerp(y,target_y,_speed);

camera_set_view_size(camera,_width,_height);
camera_set_view_pos(camera,x,y);

