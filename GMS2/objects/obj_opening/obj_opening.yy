{
    "id": "5e17cc4d-464b-491e-b1f6-195b0b9340cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_opening",
    "eventList": [
        {
            "id": "7a9c513a-34f4-4621-a109-290b163e5e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        },
        {
            "id": "ed4c171c-009a-4df5-a5e0-6d77cdfecd48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        },
        {
            "id": "3c79cc3a-a87f-44f4-92cb-30fc9bf53d8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}