if(end_alpha < 1){
	draw_set_alpha(end_alpha);
}else{
	draw_set_alpha(alpha);
}

//Draw opening screen
draw_sprite_stretched(spr_open,0,0,0,display_get_gui_width(),display_get_gui_height());

if(end_alpha == 1){
	if(alpha==1){
		//Hack the continue prompt
		draw_set_alpha(sin(timer));
	}else{
		draw_set_alpha(1);
	}

	draw_set_colour(c_black);
	draw_rectangle(
		display_get_gui_width() - 400,
		display_get_gui_height() - 200,
		display_get_gui_width(),
		display_get_gui_height(),
		false
	);
draw_set_alpha(1);
}
