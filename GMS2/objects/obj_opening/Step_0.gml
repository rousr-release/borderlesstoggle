if(!fade){
	if(alpha < 1){
		alpha+=0.02/2;
	}else{
		alpha = 1;
		timer+=0.02;
	
		if(sin(timer) < 0 ){
			timer = 0;
		}
	
		if(keyboard_check_pressed(vk_space)){
			fade = true;
			game.paused = false;
		}

	}
}else{
	end_alpha -= 0.02;
	if(end_alpha < 0.2){
		instance_create_depth(0,0,-1,obj_menu);
		instance_destroy();
	}
}