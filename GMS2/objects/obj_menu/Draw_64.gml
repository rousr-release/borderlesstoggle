//Only when paused
if(!game.paused){
	exit;
}
var _container_bounds = [(display_get_gui_width() - 400) /2 ,(display_get_gui_height() - 200) /2,400,200];
var _text_padding = 32;

//Draw overlay
draw_set_colour(c_black);
draw_set_alpha(0.8);
draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);

//Draw container
draw_set_alpha(1);
draw_sprite_stretched(spr_menu,0,_container_bounds[0],_container_bounds[1],_container_bounds[2],_container_bounds[3]);

//Draw menu items
draw_set_alpha(1);
draw_set_colour(c_white);
draw_set_halign(fa_center);
draw_set_valign(fa_top);
draw_set_font(fnt_24px);



var _x = _container_bounds[0] + _container_bounds[2]/2;// x + width /2
var _y = _container_bounds[1] + _text_padding;//y + padding 

var _i=0;
repeat(array_length_1d(menu_items)){
	var _string = menu_items[_i];
	
	if(is_array(options[_i])){
		var _options = options[_i];
		_string = _options[options_index[_i]];	
	}
	
	draw_set_colour(c_white);
	
	if(menu_item_index == _i){
		draw_set_colour(c_yellow);
		if(has_arrows[_i]){
			_string = "<-"+_string+"->";
		}else{
			_string ="-"+_string+"-";
		}
	}
	
	if(	_i == 2 && ( game.camera.window_mode == window_modes.fullscreen || game.camera.window_mode == window_modes.borderless_fullscreen)){
		draw_set_colour(c_gray);	
	}
	
	
	draw_text(_x, _y + (_text_padding * _i),_string);
	++_i;
}

draw_set_colour(c_white);
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);
draw_set_font(fnt_14px);
draw_text(display_get_gui_width()-3,display_get_gui_height()-3,"Extension by Rousr - Demo project by @net8floz");