//Always available
if(keyboard_check_pressed(vk_escape)){
	game.paused = !game.paused;
}

//Only when paused
if(!game.paused){
	exit;
}

var _update_resolution = false,
	_update_window_mode = false;


if(keyboard_check_pressed(vk_up)){
	--menu_item_index;
	if(	menu_item_index == 2 && ( game.camera.window_mode == window_modes.fullscreen || game.camera.window_mode == window_modes.borderless_fullscreen)){
		--menu_item_index;
	}
}

if(keyboard_check_pressed(vk_down)){
	++menu_item_index;
	if(	menu_item_index == 2 && ( game.camera.window_mode == window_modes.fullscreen || game.camera.window_mode == window_modes.borderless_fullscreen)){
		++menu_item_index;
	}
}

if(menu_item_index >= array_length_1d(menu_items)){
	menu_item_index = 0;
}

if(menu_item_index < 0){
	menu_item_index = array_length_1d(menu_items)-1;
}

if(keyboard_check_pressed(vk_left)){
	if(is_array(options[menu_item_index])){
		options_index[@ menu_item_index]-=1;
		if(options_index[menu_item_index] < 0){
			options_index[@ menu_item_index] = array_length_1d(options[menu_item_index])-1;
		}
		if(menu_item_index == 2){
			_update_resolution = true;
		}else{
			_update_window_mode = true;
		}
	}
}

if(keyboard_check_pressed(vk_right)){
	if(is_array(options[menu_item_index])){
		options_index[@ menu_item_index]+=1;
		if(options_index[menu_item_index] >= array_length_1d(options[menu_item_index])){
			options_index[@ menu_item_index] = 0;
		}
		if(menu_item_index == 2){
			_update_resolution = true;
		}else{
			_update_window_mode = true;
		}
	}
}


//On enter 
if(keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter)){
	switch(menu_item_index){
		case 0:
			game.paused = !game.paused;
		break;
		case 3:
			game_end();
		break;
	}
}

//Horizontal options
if(_update_resolution){
	var _options = options[menu_item_index];
	menu_set_resolution(_options[options_index[menu_item_index]]);
}

if(_update_window_mode){
	var _options = options[menu_item_index];
	menu_set_window_mode(_options[options_index[menu_item_index]]);
}

if(	menu_item_index == 2 && ( game.camera.window_mode == window_modes.fullscreen || game.camera.window_mode == window_modes.borderless_fullscreen)){
	++menu_item_index;
}
