//Warning!! Do not look to this object for how to write a good menu system.
//This is how to write a menu system quickly 

menu_items[3] = "Quit";
menu_items[2] = "1280x720";
menu_items[1] = "Borderless Window";
menu_items[0] = "Resume";

has_arrows[3] = false;
has_arrows[2] = true;
has_arrows[1] = true;
has_arrows[0] = false;

options[3] = false;
options[2] = ["640x360","800x600","1280x720","1360x768","1440x900","1600x900","1920x1080"];
options[1] = ["Borderless Window", "Window", "Borderless Fullscreen", "Fullscreen"];
options[0] = false;

options_index[3] = 0;
options_index[2] = 2;
options_index[1] = 0;
options_index[0] = 0;

menu_item_index = 0;